const https = require('https');

const options = {
  hostname: 'io.brie.ninja',
  path: '/omg.html',
  headers: { 'User-Agent': 'kubernetes-cron-job/v0' }
};

https.get(options, function(res) {
  console.log('statusCode:', res.statusCode);
  console.log('headers:', res.headers);
});

