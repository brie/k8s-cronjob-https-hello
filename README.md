# HOWTO: Run a small Node script as a Kubernetes CronJob

[Read this post](https://icanhazk8s.com/post/howto-k8s-cronjob-with-nodejs-https-hello/) on `icanhazk8s.com`. 

## About CronJobs in Kubernetes

The time you spent memorizing `cron` syntax will be of use! Old style cron jobs ran scripts; Kubernetes CronJobs execute Docker images. 

## HOWTO

In this post, I:

  - Wrote a small Node script that makes a call to a Web server and prints the response headers
  - Built a Docker image that runs that script
  - Created a CronJob to run that Docker image once every minute
  - Monitored the cron job


### Small Node script

The entire `https-hello.js` follows:

```
const https = require('https');

const options = {
  hostname: 'io.brie.ninja',
  path: '/omg.html',
  headers: { 'User-Agent': 'kubernetes-cron-job/v0' }
};

https.get(options, function(res) {
  console.log('statusCode:', res.statusCode);
  console.log('headers:', res.headers);
});

```

It makes a call to `https://io.brie.ninja/omg.html` and prints the status code and HTTP response headers. 

### Put the script into a Docker image

I wrote this `Dockerfile`:

```
FROM node:12

WORKDIR /usr/src/app

COPY src/https-hello.js ./

CMD ["node","https-hello.js"]
```

Once `docker build` succeeds, running this Docker container will run the Node script that reaches out to `https://io.brie.ninja/omg.html`. 

You can configure `docker` to build your image directly into the [in-cluster Docker daemon](https://minikube.sigs.k8s.io/docs/handbook/pushing/#1-pushing-directly-to-the-in-cluster-docker-daemon-docker-env). It's deceptively simple:

  - `eval $(minikube docker-env)`
  - `docker build -t https-hello:v0 .`

When you run `docker image ls`, you will see your newly created Docker image as well as the other Docker images available to the in-cluster Docker daemon. It's time to configure a cron job to run this container.  


### Create and configure CronJob

You can write the YAML for the CronJob by hand or using a combination of `--dry-run` and `-o=yaml` like so:

```
kubectl  create cronjob simplejob --image=httpshello:v0 --schedule="*/1 * * * *"     --dry-run=client      -o=yaml | tee https-hello-cronjob.yaml
```

I set `imagePullPolicy` to `IfNotPresent` in `https-hello-cronjob.yaml` and we are all ready to go. Instead of running `crontab -e`, we instead do `kubectl apply -f https-hello-cronjob.yaml`. To make sure that everything looks OK, instead of `crontab -l`, use:

```
kubectl get cronjobs
```

## Monitoring CronJobs in Kubernetes

**Tired**: `tail -F /var/log/syslog | grep CRON`

**Wired**: `kubectl  get jobs --watch`

In `k9s`, use `:cj` to view cluster cron jobs. 


## More

There is a difference between Jobs and CronJobs. [readmore](https://linuxhint.com/kubernetes-jobs-and-cron-jobs/)

Pods that run CronJobs complete their work and exit. Upon inspection, their status is **Completed**. 

A guided reading journey:

  - [Running Automated Tasks with a CronJob](https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/)
  - [Create new CronJob from existing CronJob](https://howchoo.com/kubernetes/how-to-create-a-kubernetes-job-from-a-cron-job) 
  - [How To Use Kubernetes' Job and CronJob](https://medium.com/better-programming/tutorial-how-to-use-kubernetes-job-and-cronjob-1ef4ffbc8e84)
  - [How we learned to improve Kubernetes CronJobs at Scale (Part 1 of 2)](https://eng.lyft.com/improving-kubernetes-cronjobs-at-scale-part-1-cf1479df98d4)
