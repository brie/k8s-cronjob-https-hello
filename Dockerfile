FROM node:12

WORKDIR /usr/src/app

COPY src/https-hello.js ./

CMD ["node","https-hello.js"]
